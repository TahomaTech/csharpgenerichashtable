﻿using System;
using System.Collections.Generic;

namespace CSharpGenericHashTable.Interfaces
{
    /// <summary>
    /// An interface for GenericHashTable to use.
    /// </summary>
    /// <typeparam name="T">Generic data type.</typeparam>
    public interface IGenericHashTable<T> : IEnumerable<T>
    {
        /// <summary>
        /// Adds an array of elements to the HashTable.
        /// </summary>
        /// <param name="elements">Array of elements to add to the HashTable.</param>
        public void Add(T[] elements);

        /// <summary>
        /// Adds an element to the HashTable.
        /// </summary>
        /// <param name="element">Element to add to the HashTable.</param>
        private void Add(T element) { }

        /// <summary>
        /// Clears the HashTable.
        /// </summary>
        public void Clear();

        /// <summary>
        /// Checks if the HashTable contains the element.
        /// </summary>
        /// <param name="element">Element to check if it's in the HashTable.</param>
        public bool Contains(T element);

        /// <summary>
        /// Returns an element that matches the input, according to equals().
        /// </summary>
        /// <param name="element">Element to return data of.</param>
        /// <returns>A matching element, if one exist.</returns>
        public T Get(T element);

        /// <summary>
        /// Tells us if the HashTable is empty or not.
        /// </summary>
        /// <returns>True if HashTable is empty.</returns>
        public bool IsEmpty();

        /// <summary>
        /// Removes an element from the HashTable.
        /// </summary>
        /// <param name="element">Element to remove from the HashTable.</param>
        public void Remove(T element);

        /// <summary>
        /// Tells us how many elements are in the HashTable.
        /// </summary>
        /// <returns>The size of the HashTable.</returns>
        public int Size();

        /// <summary>
        /// Returns a generic enumerator.
        /// </summary>
        /// <returns>A generic enumerator.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            throw new NotImplementedException();
        }

    }
}
