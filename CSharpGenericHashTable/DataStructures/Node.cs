﻿using System;

namespace CSharpGenericHashTable.DataStructures
{
	/// <summary>
    /// Internal Node class that lets us use separate chaining in the HashTable.
    /// </summary>
    /// <typeparam name="T">Generic data type</typeparam>
	internal class Node<T>
	{
		// Fields
		internal T data { get; set; }
		internal Node<T> next { get; set; }

		/// <summary>
        /// Constructor that takes a data type of T and makes the next Node null.
        /// </summary>
        /// <param name="data"></param>
		public Node(T data)
		{
			this.data = data;
			this.next = null;
		}

        public override string ToString()
        {
            return $"NODE: {{data = {data}, next = {next}}}";
        }
    }
}