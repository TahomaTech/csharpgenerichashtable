﻿using System;
using CSharpGenericHashTable.DataStructures;

namespace CSharpGenericHashTable
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Generic HashTable informal testing!\n");

            Console.WriteLine("Making a small hash table to force a resize...");
            GenericHashTable<int> table = new GenericHashTable<int>(2);
            Console.WriteLine("Small hash table created!\n");

            var ints = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            Console.WriteLine("Adding ints to hash table...");
            table.Add(ints);
            Console.WriteLine($"Successfully added {table.Size()} ints to hash table!\n");

            Console.WriteLine($"Is the hash table empty? {table.IsEmpty()}");
            Console.WriteLine("Clearing the hash table...");
            table.Clear();
            Console.WriteLine($"Hash table was successfully cleared? {table.IsEmpty()}\n");

            Console.WriteLine("Adding more ints...");
            for (int i = 0; i < 50; i++)
            {
                table.Add(new int[] { i });
            }
            Console.WriteLine($"Successfully added {table.Size()} ints to hash table!\n");

            Console.WriteLine($"Does our hash table contain a 3? {table.Contains(3)}");
            Console.WriteLine($"Does our hash table contain a 6? {table.Contains(6)}");
            Console.WriteLine($"Does our hash table contain a 9? {table.Contains(9)}\n");

            Console.WriteLine($"Testing get method by calling Get(5)... {table.Get(5)}");

            Console.WriteLine("Testing remove...");
            Console.WriteLine($"Removing 4...");
            table.Remove(4);
            Console.WriteLine("Removing 6....");
            table.Remove(6);
            Console.WriteLine($"Succesfully removed 4? {!table.Contains(4)}");
            Console.WriteLine($"Succesfully removed 6? {!table.Contains(6)}\n");

            Console.WriteLine($"New table size: {table.Size()}\n");

            Console.WriteLine(table.ToString());

            Console.WriteLine("Testing the enumerator...");
            foreach (var item in table)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine("\nEnumerator is finished!");
        }
    }
}
